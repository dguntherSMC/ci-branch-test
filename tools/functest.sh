#!/bin/bash

. tools/response

echo "These are functional tests... ${EXITCODE}... They take a long time to run, so we only require them when merging back to the main branch"
echo 
if [ $EXITCODE != "0" ]; then
  echo "Fail is set ${EXITCODE}"
fi

exit ${EXITCODE}

